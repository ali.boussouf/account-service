package com.yourbank.account;

import java.util.Set;
import java.util.concurrent.CompletionStage;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import io.smallrye.mutiny.Uni;

@Path("/api/v1/accounts")
public class AccountsResource {

    @Inject
    @RestClient
    AccountsService accountsService;

    @GET
    @Path("/id/{id}")
    public Set<Account> id(@PathParam String id) {
        return accountsService.getById(id);
    }

    @GET
    @Path("/id-async/{id}")
    public CompletionStage<Set<Account>> idAsync(@PathParam String id) {
        return accountsService.getByIdAsync(id);
    }

    @GET
    @Path("/id-uni/{id}")
    public Uni<Set<Account>> idMutiny(@PathParam String id) {
        return accountsService.getByIdAsUni(id);
    }
}
