package com.yourbank.account;

import java.util.Set;
import java.util.concurrent.CompletionStage;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import io.smallrye.mutiny.Uni;

@Path("/api/v1/accounts")
@RegisterRestClient
@RegisterClientHeaders(RequestUUIDHeaderFactory.class)
public interface AccountsService {

    @GET
    Set<Account> getById(@QueryParam String id);

    @GET
    CompletionStage<Set<Account>> getByIdAsync(@QueryParam String id);

    @GET
    Uni<Set<Account>> getByIdAsUni(@QueryParam String id);
}
